package com.pleshkov.websocket;

import com.pleshkov.Main;
import com.pleshkov.generator.UniqueGenerator;
import com.pleshkov.util.Constants;
import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.framing.CloseFrame;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.handshake.ServerHandshakeBuilder;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniil Pleshkov
 */
public class GenerationServer extends WebSocketServer {

    private final Object lock;

    private final UniqueGenerator generator;

    private final Set<String> ipsInUse;

    public GenerationServer(int port) throws UnknownHostException {
        super(new InetSocketAddress(port));
        lock = new Object();
        generator = new UniqueGenerator();
        ipsInUse = new HashSet<>();
    }

    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    @Override
    public ServerHandshakeBuilder onWebsocketHandshakeReceivedAsServer(WebSocket conn, Draft draft,
                                                                       ClientHandshake request) throws InvalidDataException {
        ServerHandshakeBuilder builder = super
                .onWebsocketHandshakeReceivedAsServer(conn, draft, request);

        synchronized (lock) {
            var ip = conn.getRemoteSocketAddress().getAddress().getHostAddress();
            if (ipsInUse.contains(ip)) {
                LOGGER.log(Level.INFO, "Refuse connection for ip: " + ip);
                throw new InvalidDataException(CloseFrame.REFUSE, "More than one connection from the same IP is not allowed!");
            }
            ipsInUse.add(ip);
        }

        return builder;
    }


    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {

        LOGGER.log(Level.INFO, "New connection with: " + webSocket.getRemoteSocketAddress().getAddress().getHostAddress());
    }

    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {

        LOGGER.log(Level.INFO, "Close connection with" + webSocket.getRemoteSocketAddress().getAddress().getHostAddress());
        synchronized (lock) {
            ipsInUse.remove(webSocket.getRemoteSocketAddress().getAddress().getHostAddress());
        }
    }

    @Override
    public void onMessage(WebSocket webSocket, String s) {

        LOGGER.log(Level.INFO, "Message from" + webSocket.getRemoteSocketAddress().getAddress().getHostAddress());
        webSocket.send(Constants.JSON_RESPONSE_TO_FORMAT.formatted(generator.generateLong()));
    }

    @Override
    public void onError(WebSocket webSocket, Exception e) {

        LOGGER.log(Level.INFO, "Error: " + e.toString());
        webSocket.close();
    }

    @Override
    public void onStart() {

        LOGGER.log(Level.INFO, "Start");
    }
}
