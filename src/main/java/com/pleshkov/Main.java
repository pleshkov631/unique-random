package com.pleshkov;

import com.pleshkov.websocket.GenerationServer;
import org.java_websocket.server.WebSocketServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniil Pleshkov
 */
public class Main {


    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws InterruptedException {
        int port = 8887; // 8887 is the port the server will listen to
        try {
            WebSocketServer server = new GenerationServer(port);
            server.start();
            LOGGER.log(Level.INFO, "WebSocket server started on port: " + server.getPort());
            BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                String in = sysin.readLine();
                if (in.equals("exit")) {
                    server.stop(1000);
                    break;
                }
            }
        } catch (IOException | InterruptedException e) {
            LOGGER.log(Level.SEVERE, "Exception occurred", e);
        } finally {
            LOGGER.log(Level.INFO, "WebSocket server stopped");
        }
    }
}
