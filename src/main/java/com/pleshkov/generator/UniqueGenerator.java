package com.pleshkov.generator;

import com.google.common.hash.BloomFilter;

import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniil Pleshkov
 */
public class UniqueGenerator {

    private static final Logger LOGGER = Logger.getLogger(UniqueGenerator.class.getName());

    //TODO: extract to configuration params
    public static final int POSSIBLE_NUM_OF_GENERATIONS = 100_000;
    public static final double ACCURATE_VALUE = 0.1;

    // Not thread-safe bloom filter
    // <a href="https://www.geeksforgeeks.org/bloom-filters-introduction-and-python-implementation/">More about bool filter structure</a>
    private final BloomFilter<Long> bloomFilter = BloomFilter.create(
            com.google.common.hash.Funnels.longFunnel(),
            POSSIBLE_NUM_OF_GENERATIONS,
            ACCURATE_VALUE);

    private final Object sync = new Object();

    public Long generateLong() {

        long random = ThreadLocalRandom.current().nextLong();
        synchronized (sync) {
            while (bloomFilter.mightContain(random)) {
                LOGGER.log(Level.INFO, "Skip possibly generated long " + random);
                random = ThreadLocalRandom.current().nextLong();
            }
            bloomFilter.put(random);
            LOGGER.log(Level.INFO, "Generated long: " + random);
        }
        return random;
    }
}
