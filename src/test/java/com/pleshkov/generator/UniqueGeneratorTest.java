package com.pleshkov.generator;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CyclicBarrier;
import java.util.stream.IntStream;

/**
 * @author Daniil Pleshkov
 */
class UniqueGeneratorTest {

    @Test
    void generateLongConcurrencyTest() throws InterruptedException {

        ConcurrentHashMap<Long, Integer> generatedLongs = new ConcurrentHashMap<>();
        var uniqueGenerator = new UniqueGenerator();
        int numberOfThreads = 1000;
        CyclicBarrier barrier = new CyclicBarrier(numberOfThreads);
        var threads = IntStream.range(0, numberOfThreads).mapToObj(__ -> new Thread(() -> {
            try {
                barrier.await();
                while (!Thread.interrupted()) {
                    var random = uniqueGenerator.generateLong();
                    generatedLongs.compute(random, (k, v) -> v == null ? 1 : v + 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        })).toList();
        threads.forEach(Thread::start);

        Thread.sleep(1_000);

        threads.forEach(Thread::interrupt);

        System.out.printf("Generated %d longs%n", generatedLongs.size());
        Assertions.assertThat(generatedLongs.values()).allMatch(v -> v == 1);
    }
}
