# Project Description

This project is a WebSocket server application written in Java.
It uses the Java-WebSocket library to create a server that listens for connections from WebSocket clients.
The server only allows one connection per IP address.
The server also generates unique long numbers using a Bloom filter. This ensures that the same number is not generated twice.
The generated number is then sent back to the client as a response to any message received from the client.

# Prerequisites

To run this project, you need the following:

- Java Development Kit (JDK) 17 or later

# How to Run

1. Clone the repository to your local machine.
2. Navigate to the project directory in your terminal.
3. Run the command `java -jar artifacts/unique-random-1.0.0.jar` to start the server. 
4. The server will start and listen on port 8887. You can connect to it using any WebSocket client.
5. Send any message to the server, and it will respond with a unique long number.
6. Run exit in terminal to stop server.

